extends Node2D

func _ready():
    $AdditionPreview.set_base_number(5)
    $AdditionPreview.add_factor(7)
    $AdditionPreview.add_factor(2.5)
