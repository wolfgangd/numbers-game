extends Node2D

signal pressed

var annotation = ""
var suffix = ""

const Pulser = preload("res://util/Pulser.gd")
var pulser

func _ready():
    $Hover.visible = false
    annotate("")
    enable(false)

func _enter_tree():
    pulser = Pulser.new(self, $Button.rect_size, 0.15, 0.5)

func _process(_delta):
    $Annotation.text = "%s %s" % [annotation, suffix]
    
func enable(b):
    if b && $Button.disabled:
        pulser.pulse()
        $Disabled.visible = false
        $Button.disabled = false
    if !b && !$Button.disabled:
        $Disabled.visible = true
        $Button.disabled = true
    
func annotate_suffix(text):
    suffix = text
        
func annotate(text):
    annotation = text
        
func set_progress(ap, progress):
    var prev_ap = progress[0]
    var next_ap = progress[1]
    var rel_ap = ap - prev_ap
    var fraction = normalize_progress(rel_ap, prev_ap, next_ap)
    $Progress.set_size(Vector2(fraction*50, 5))

func normalize_progress(rel_ap, prev_ap, next_ap):
    var size = next_ap - prev_ap
    if size == 0:
        return 1
    return clamp(float(rel_ap)/(next_ap-prev_ap), 0, 1)
    
func _on_Button_mouse_entered():
    if !$Button.disabled:
        $Hover.visible = true
    
func _on_Button_mouse_exited():
    $Hover.visible = false

func set_texture(path):
    var texture = load(path)
    $Button.texture_normal = texture
    $Button.texture_disabled = texture
    $Button.texture_hover = texture

func _on_Button_pressed():
    emit_signal("pressed")
