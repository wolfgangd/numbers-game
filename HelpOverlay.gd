extends CanvasLayer

var abilities_gui
var gui

var page_index
var pages

var ability_help
var grid_help
var total_help
var status_help


func init(gui_):
    gui = gui_
    abilities_gui = gui.get_node("AbilitiesGUI")
    visible = false
    
func _enter_tree():
    var vp = get_viewport()
    $Background.set_position(Vector2(0, 0))
    $Background.set_size(vp.size)
    
    var diagonal_move_help = add_help("Enables diagonal moves. Select target with mouse.")
    var factor_help = add_help("Multiplies digit by a factor.")
    var mult_help = add_help("Chance to multiply TOTAL by digit. Among other things.")
    var same_values_help = add_help("Add all cells with the same value.")
    var prime_help = add_help("If TOTAL is a prime, multiply it by a factor. Otherwise half it. Limited time to guess!")
    grid_help = add_help("Move your place in the grid with cursor keys. " + 
                        "\nAny accumulated value while on a cell will be added after moving.")
    total_help = add_help("This is total value accumlated by grid movements and abilities. " + 
                          "\nBelow, a preview of the next addition is shown.")
    status_help = add_help("Moves: moves you have left. 0 means game over." + 
                            "\nAP: Ability points requird to use abilities. You get some amount back per move." + 
                            "\nHigh: Highest total achieved for this grid configuration.")
                            
    ability_help = {
        "ButtonDiagonal": diagonal_move_help,
        "ButtonFactor": factor_help,
        "ButtonMultiply": mult_help,
        "ButtonSameValues": same_values_help,
        "ButtonPrime": prime_help
    }
    
    pages = [
        [diagonal_move_help, factor_help, mult_help, same_values_help, prime_help],
        [grid_help],
        [total_help],
        [status_help]
    ]

func add_help(text):
    var help = $HelpTemplate.duplicate()
    help.set_text(text)
    add_child(help)
    return help
    
func show():
    visible = true
    for page in pages:
        for help in page:
            help.visible = false

    page_index = 0
    show_page(true)
    
func hide():
    page_index = 0
    visible = false

func next():
    show_page(false)
    page_index = (page_index + 1)%pages.size()
    show_page(true)
    
func show_page(b):
    for help in pages[page_index]:
        help.visible = b
        

func _on_Main_grid_initialized(grid_position, _grid_dimensions):
    for button in ability_help:
        place_ability_help(button, ability_help[button])
    grid_help.position = grid_position + Vector2(0, 50)
    grid_help.grow_horizontally_by(150)                        

    var total = gui.get_node("Total")
    var total_pos = total.get_global_transform().get_origin()
    total_help.grow_horizontally_by(50)
    total_help.set_position(total_pos - Vector2(total_help.get_size().x + 5, 0))
    
    var remaining_moves = gui.get_node("RemainingMoves")
    var rm_pos = remaining_moves.get_global_transform().get_origin()
    status_help.grow_horizontally_by(100)
    status_help.set_position(rm_pos - Vector2(status_help.get_size().x + 5, 0))

    

            
func place_ability_help(button_name, help):
    var button = abilities_gui.get_node(button_name)
    var pos = button.get_global_transform().get_origin()
    help.visible = false
    help.set_position(pos + Vector2(55, 0))
    
