extends CanvasLayer

signal start_new_game

func _ready():
    visible = false
    
func _enter_tree():
    var vp = get_viewport()
    $Background.set_position(Vector2(0, 0))
    $Background.set_size(vp.size)
    var background = $Dialog.get_node("Background")
    var bg_width = background.get_size().x
    var bg_height = background.get_size().y
    $Dialog.position = Vector2(vp.size.x/2 - bg_width/2, vp.size.y/2 - bg_height/2)

func _on_Main_game_over(current_seed):
    $Dialog/ButtonRetry.disabled = current_seed == -1
    visible = true

func _on_ButtonNewGame_pressed():
    visible = false
    emit_signal("start_new_game", false)


    
func _on_ButtonRetry_pressed():
    visible = false
    emit_signal("start_new_game", true)
