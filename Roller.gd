extends Node2D

const Pulser = preload("res://util/Pulser.gd")
signal roller_effect_started
signal roller_effect_finished

var resource_paths
var offset 
var texture_size = Vector2(50, 50)
var textures = []
var pulser

func set_position(pos):
    position = pos
    pulser = Pulser.new(self, $ReferenceRect.rect_size, 0.15, 0.5)
    
func init(resource_paths_):
    resource_paths = resource_paths_
    textures = []
    for path in resource_paths:
        textures.append(load(path))
    reset()

func reset():
    for child in $ReferenceRect.get_children():
        $ReferenceRect.remove_child(child)
    offset = 0
    for _i in range(10):
        for tex in textures:
            add_texture(tex, offset)
            offset-= texture_size.y
        

func add_texture(tex, y_offset):
    var texture = TextureRect.new()
    texture.texture = tex
    texture.expand = true
    texture.rect_size = texture_size
    texture.stretch_mode = TextureRect.STRETCH_SCALE
    texture.mouse_filter = TextureRect.MOUSE_FILTER_IGNORE
    texture.set_position(Vector2(0, y_offset))
    $ReferenceRect.add_child(texture)
    

func roll(wanted_outcome, data = null):
    for i in range(0, textures.size()):
        add_texture(textures[i], offset)
        offset-= texture_size.y
        if i == wanted_outcome:
            break

    var tween = create_tween()
    var children = $ReferenceRect.get_children()
    for texture in children:
        var pos = texture.get_position()
        tween.set_ease(Tween.EASE_IN_OUT)
        tween.set_trans(Tween.TRANS_EXPO)
        tween.parallel().tween_method(texture, "set_position", pos, pos + Vector2(0, (children.size()-1)*50), 2)
    tween.tween_callback(self, "on_roll_finished", [data])
    emit_signal("roller_effect_started")

func on_roll_finished(data):
    pulser.pulse()
    emit_signal("roller_effect_finished", data)
