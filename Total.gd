extends Node2D

const Pulser = preload("res://util/Pulser.gd")
var pulser

func init():
    update_value(0)

func _enter_tree():
    pulser = Pulser.new(self, $Label.rect_size, 0.15, 0.7)
    

func update_value(value):
    $Label.text = str(value)
    pulser.pulse()

