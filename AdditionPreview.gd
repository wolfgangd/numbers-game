extends Node2D

var passive_total

func _ready():
    init()
    
func init():
    passive_total = 0
    $Plus.visible = false
    $PassiveTotal.visible = false
    for c in $Factors.get_children():
        $Factors.remove_child(c)
    
func set_plus(digit, factor):
    $Plus.visible = true
    var suffix = ""
    if factor > 1: suffix = " * %s" % str(factor) 
    $Plus.text = "+ %s%s" % [str(digit), suffix]
    var pos = Vector2(100, 0)
    $Plus.set_position(pos)
    slide_in($Plus)

    
func add_factor(value):    
    var label = $FactorLabelTemplate.duplicate()
    label.visible = true
    var pos = Vector2(100, 0)
    var cc = $Factors.get_child_count()
    if cc > 0:
        pos = $Factors.get_children()[cc -1].get_position() + Vector2(0, 20)
    if typeof(value) == TYPE_INT:
        label.text = "* %d" % value
    else:
        label.text = "* %.1f" % value

    label.set_position(pos)
    $Factors.add_child(label)
    slide_in(label)
    

func render_passive_total():    
    if passive_total == 0: return
    $PassiveTotal.visible = true
    var pos = Vector2(0, 0)
    var cc = $Factors.get_child_count()
    if cc > 0:
        pos = $Factors.get_children()[cc -1].get_position() + Vector2(0, 20)
    
    if $Plus.visible:
        pos += $Plus.get_position() + Vector2(0, 20)
        
    $PassiveTotal.text = "+ %d" % passive_total
    $PassiveTotal.set_position(pos)
    
    slide_in($PassiveTotal)

func slide_in(label):
    var tween = create_tween()
    var from_pos = label.get_position()
    tween.set_ease(Tween.EASE_OUT)
    tween.set_trans(Tween.TRANS_BOUNCE)
    tween.tween_method(label, "set_position", from_pos, from_pos - Vector2(100, 0), 0.5)
