extends Node2D


func set_text(text):
    $Label.text = text

func grow_horizontally_by(x):
    $Label.set_size($Label.get_size() + Vector2(x, 0))

func get_size():
    return $Label.get_size()
