extends Node2D

class_name Cell

signal cell_clicked

var digit = 0
var selected = false
var used = false
var reserved = false
var clickable = false
var orig_position
var orig_scale

enum {OP_ADD, OP_MULTIPLY}

var op = OP_ADD
var digit_factor = 1
var total_factors = []

const SELECTION_SCALE = 0.4

const Pulser = preload("res://util/Pulser.gd")

func _ready():
    $Label.text=str(digit)
    reset()

func reset():
    used = false
    reserved = false
    selected = false
    digit_factor = 1
    total_factors = []
    op = OP_ADD
    $Background.get_node("Shadow").visible = false
    $Overlay.hide_indicator()


func _process(_delta):
    $SelectionIndicator.visible = selected
    $UsedIndicator.visible = used

func make_clickable(b):
    if clickable == b: return
    clickable = b
    if clickable:
        enlarge()
    else:
        var shadow = $Background.get_node("Shadow")
        position = orig_position
        scale = Vector2(1, 1)
        shadow.visible = false
        z_index = 0
        shadow.z_index = 0
 
func get_size():
    return $Background.get_size()
           
func set_op(new_op):
    if new_op == OP_ADD:
        total_factors = []
    op = new_op

func set_factor(new_factor):
    if digit_factor == 1:
        digit_factor = new_factor
    else:
        digit_factor += new_factor
    $Overlay.set_indicator("*%d" % digit_factor)
    
func add_total_factor(f):
    total_factors.append(f)

func mark_as_same_value():
    $Overlay.set_indicator("+")
    reserved = true
    enlarge()
    
func enlarge():
    var shadow = $Background.get_node("Shadow")
    orig_position = position
    orig_scale = scale
    var size = $Background.get_size()
    position = orig_position - Vector2(size.x*SELECTION_SCALE/2, size.y*SELECTION_SCALE/2)
    $Tween.interpolate_property(self, 
                                "scale",
                                orig_scale, 
                                Vector2(1+SELECTION_SCALE, 1+SELECTION_SCALE), 
                                0.15,
                                Tween.TRANS_LINEAR, 
                                Tween.EASE_IN_OUT)
    $Tween.start()
    shadow.position = Vector2(size.x * 0.15, size.y * 0.15)
    shadow.visible = true
    z_index = 10
    shadow.z_index = 5

func execute_op(total):
    var total_value = total
    print("--- digit_factor ", digit_factor)
    print("--- total_factors.A ", total_factors)
    match op:
        OP_ADD: 
            if total_factors.empty():
                return total + digit*digit_factor
            else:
                if digit_factor > 1:
                    total_factors.append(digit_factor)
                print("----- ", total_factors)
        OP_MULTIPLY:
            total_factors.append(digit*digit_factor)
    for f in total_factors:
        total_value *= f
    print("total_value ", total_value)
    return round(total_value)
            
                
func _on_Background_gui_input(event):
    if event is InputEventMouseButton && !event.pressed:
        emit_signal("cell_clicked")

