extends Node

signal grid_initialized
signal total_changed
signal remaining_moves
signal diagonal_move_selection
signal diagonal_move_completed
signal highscore_changed
signal game_over
signal selected_cell_changed
signal selected_cell_updated


var total = 0
var remaining_moves

enum {STATE_GAME, STATE_DIAGONAL_SELECTION, STATE_NEW_GAME, STATE_EFFECT, STATE_HELP}

var state = STATE_GAME
var selection_targets = []

var high_score = 0

const grid_size = 8
const initial_moves = 64

var current_seed
var selected_cell
var selected_cell_coords
var prev_state

func _ready():  
    current_seed = -1      
    start(false)
    
func start(reuse_grid):
    $MultRoller.init([
        "res://icons/bonus_normal.png",
        "res://icons/plus_normal.png",
        "res://icons/radioactivity_normal.png"       ])
    $MultRoller.visible = false

    $Abilities.start()
    state = STATE_GAME
    total = 0
    remaining_moves = initial_moves
    if !reuse_grid:
        current_seed = Time.get_unix_time_from_system()*1000
        high_score = 0
        emit_signal("highscore_changed", 0)
    seed(current_seed)
    var grid_dimensions = $Grid.init(grid_size, grid_size)
    emit_signal("grid_initialized", $Grid.position, grid_dimensions)    
    randomize()
    selected_cell_coords = [0, 0]
    selected_cell =  $Grid.get_cell(selected_cell_coords)
    emit_signal("selected_cell_changed", selected_cell)
    emit_state_signals()

func _unhandled_input(event):
    match state:
        STATE_HELP:
            if help_close(event):
                $GUI/HelpOverlay.hide()
                state = prev_state
            elif is_any_key_press(event):
                $GUI/HelpOverlay.next()
        STATE_GAME:
            if help_open(event):
                prev_state = state
                $GUI/HelpOverlay.show()
                state = STATE_HELP
                
            if remaining_moves > 0:
                if event.is_action_pressed("ui_left"):
                    $Grid.move_selection(0, -1)
                if event.is_action_pressed("ui_right"):
                    $Grid.move_selection(0, 1)
                if event.is_action_pressed("ui_up"):
                    $Grid.move_selection(-1, 0)
                if event.is_action_pressed("ui_down"):
                    $Grid.move_selection(1, 0)
        STATE_DIAGONAL_SELECTION:
            pass
            
func help_open(event):
    return is_key_press(event, KEY_F1)

func help_close(event):
    return is_key_press(event, KEY_ESCAPE)

func is_key_press(event, code):
    return is_any_key_press(event) && event.scancode == code

func is_any_key_press(event):
    return event is InputEventKey and event.is_pressed()


func _on_Grid_selection_moved(from_coord, to_coord):
    var cell = $Grid.get_cell(from_coord)
    total = cell.execute_op(total)
    remaining_moves -= 1
    selected_cell = $Grid.get_cell(to_coord)
    selected_cell_coords = to_coord

    total += $Abilities.get_passive_total()
    emit_signal("selected_cell_changed", selected_cell)
    $Abilities.tick()
    if possible_moves() == 0 && total > high_score:
        high_score = total
        emit_signal("highscore_changed", high_score)
    if possible_moves() == 0:
        emit_signal("game_over", current_seed)
    emit_state_signals()
        
func emit_state_signals():
    emit_signal("total_changed", total)
    emit_signal("remaining_moves", remaining_moves)

func possible_moves():
    if remaining_moves == 0: return 0
    var moves = 0
    if $Grid.try_move(0, 1): moves += 1
    if $Grid.try_move(0, -1): moves += 1
    if $Grid.try_move(1, 0): moves += 1
    if $Grid.try_move(-1, 0): moves += 1
    return moves

func _on_cell_clicked(coords):
    if state == STATE_GAME:
        var row_offset = coords[0] - selected_cell_coords[0]
        var column_offset = coords[1] - selected_cell_coords[1]
        if row_offset in [-1, 0, 1] and column_offset in [-1, 0, 1] and (row_offset == 0 or column_offset == 0):
            $Grid.move_selection(row_offset, column_offset)
    else:
        state = STATE_GAME
        $Abilities.use_diagonal()
        $Grid.set_selection(coords)
        emit_signal("diagonal_move_completed", selection_targets)

func _on_ability_cancelled():
    state = STATE_GAME
    emit_signal("diagonal_move_completed", selection_targets)

func _on_use_ability_diagonal():
    if state == STATE_GAME:
        state = STATE_DIAGONAL_SELECTION
        var targets = $Grid.get_diagonal_neighbors()
        selection_targets = targets
        emit_signal("diagonal_move_selection", targets)

func _on_use_ability_factor():
    if state == STATE_GAME:
        $Abilities.use_factor(selected_cell)
        emit_signal("selected_cell_updated", selected_cell)

func _on_use_ability_multiply():
    if state == STATE_GAME:
        $Abilities.use_multiply(selected_cell)

func _on_use_ability_same_values():
    if state == STATE_GAME:
        var active_cells = $Grid.get_cells_with_digit_of_selected()
        $Abilities.use_same_values(active_cells) 
        emit_signal("selected_cell_updated", selected_cell)
        

func _on_use_ability_prime():
    if state == STATE_GAME:
        $Abilities.use_prime(selected_cell, total)
        emit_signal("selected_cell_updated", selected_cell)
        
func _on_NewGameDialog_start_new_game(retry):
    start(retry)

func _on_multiply_outcome(cell, outcome):
    print("Multiply outcome ", outcome)
    $MultRoller.set_position(cell.position + Vector2(2.5, 2.5))
    var outcome_index
    match outcome:
        Multiply.MULTIPLY: outcome_index = 0
        Multiply.ADD: outcome_index = 1
        Multiply.HALF: outcome_index = 2
    $MultRoller.visible = true
    $MultRoller.reset()
    $MultRoller.roll(outcome_index, cell)
    
func _on_roller_effect_started():
    state = STATE_EFFECT


func _on_roller_effect_finished(cell):
    emit_signal("selected_cell_updated", cell)
    state = STATE_GAME


