extends Node2D

const Pulser = preload("res://util/Pulser.gd")
var pulser

func init(text, initial_value):
    $Label.text = text
    update_value(initial_value)

func _enter_tree():
    pulser = Pulser.new(self, $Label.rect_size + $Number.rect_size, 0.15, 0.7)
    

func update_value(value):
    $Number.text = str(value)

func pulse():
    pulser.pulse()

