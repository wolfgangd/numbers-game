extends Node2D

signal selection_moved

export(PackedScene) var cell_scene
export var grid_gap = 5

var row_count
var column_count

var coords_to_cells = {}
var selected_coords

func init(column_count_, row_count_):
    column_count = column_count_
    row_count = row_count_
    
    for cell in coords_to_cells.values():
        remove_child(cell)
    coords_to_cells = {}
    
    
    var temp_cell = cell_scene.instance()
    var cell_size = temp_cell.get_size()
    var cell_width = cell_size[0]
    var cell_height = cell_size[1]  
    
    for row in range(row_count):
        for column in range(column_count):
            var cell = cell_scene.instance()
            cell.connect("cell_clicked", get_parent(), "_on_cell_clicked", [[row, column]])
            coords_to_cells[[row, column]] = cell
            cell.digit = randi()%10
            add_child(cell)
            cell.position = Vector2(column*(cell_width + grid_gap), row*(cell_height + grid_gap))
            
    selected_coords = [0, 0]
    get_cell(selected_coords).selected = true
    var grid_width = column_count*(cell_width + grid_gap)
    var grid_height = row_count*(cell_height + grid_gap)
    return Vector2(grid_width, grid_height)

func move_selection(row_offset, column_offset):
    var move_result = try_move(row_offset, column_offset)                        
    if (move_result):
        set_selection(move_result)

func set_selection(to):
    var prev_selected_coords = selected_coords
    selected_coords = to
    get_cell(prev_selected_coords).selected = false
    get_cell(prev_selected_coords).used = true
    get_cell(selected_coords).selected = true
    emit_signal("selection_moved", prev_selected_coords, selected_coords)


func try_move(row_offset, column_offset):
    var prev_selected_coords = [selected_coords[0], selected_coords[1]]
    var new_selected_coords = [clamp(selected_coords[0] + row_offset, 0, row_count-1), 
                               clamp(selected_coords[1] + column_offset, 0, column_count-1)]
    var new_cell = get_cell(new_selected_coords)
    if new_selected_coords != prev_selected_coords && \
        !new_cell.used && !new_cell.reserved:
        return new_selected_coords
    return null
                            
func _on_Main_diagonal_move_selection(targets):
    for target in targets:
        get_cell(target).make_clickable(true)
        
        
func _on_Main_diagonal_move_completed(targets):
    for target in targets:
        get_cell(target).make_clickable(false)

func get_diagonal_neighbors():
    var neighbors = []
    var sc = selected_coords
    for offset in [[-1, -1], [1, -1], [1, 1], [-1, 1]]:
        var coords = [sc[0] + offset[0], sc[1] + offset[1]]
        if coords[0] >= 0 && coords[0] < row_count && \
           coords[1] >= 0 && coords[1] < column_count && \
            !get_cell(coords).used:
            neighbors.append(coords)
    return neighbors

func get_cells_with_digit_of_selected():
    var selected_digit = get_selected_cell().digit
    var result = []
    for coords in coords_to_cells.keys():
        if coords == selected_coords:
            continue
        var cell = get_cell(coords)
        if cell.used:
            continue
        if cell.digit == selected_digit:
            result.append(cell)
    return result

func get_selected_cell():
    return get_cell(selected_coords)

func get_cell(coords):
    return coords_to_cells[coords]
