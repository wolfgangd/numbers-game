extends Node2D

func _ready():
    visible = false
    $Label.text = ""

func hide_indicator():
    visible = false

func set_indicator(ch):
    visible = true
    $Label.text = ch

