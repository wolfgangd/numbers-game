extends Node2D

signal use_ability_diagonal
signal use_ability_factor
signal use_ability_multiply
signal use_ability_same_values
signal use_ability_prime


func _ready():
    $ButtonDiagonal.set_texture("res://icons/larger.png")
    $ButtonMultiply.set_texture("res://icons/cross.png")
    $ButtonFactor.set_texture("res://icons/signal3.png")
    $ButtonSameValues.set_texture("res://icons/menuGrid.png")
    $ButtonPrime.set_texture("res://icons/prime.jpg")
    
func _on_ability_enabled_diagonal_move(b):
    $ButtonDiagonal.enable(b)
    
func _on_ability_enabled_same_values(b):
    $ButtonSameValues.enable(b)
    
func _on_Abilities_factor_changed(factor):
    if factor > 1:
        $ButtonFactor.annotate("*%d" % factor)
        $ButtonFactor.enable(true)
    else:
        $ButtonFactor.annotate("")
        $ButtonFactor.enable(false)

func _on_Abilities_multiply_chance_changed(value):
    if value > 0:
        $ButtonMultiply.annotate("%d%%" % value)
        $ButtonMultiply.enable(true)
    else:
        $ButtonMultiply.annotate("")
        $ButtonMultiply.enable(false)
        
func _on_same_value_max_changed(value):
    if value > 0:
        $ButtonSameValues.annotate("Max %d" % value)
    else:
        $ButtonSameValues.annotate("")

func _on_Abilities_factor_progress_changed(ap, progress):    
    $ButtonFactor.set_progress(ap, progress)

func _on_Abilities_multiply_progress_changed(ap, progress):
    $ButtonMultiply.set_progress(ap, progress)


func _on_Abilities_diagonal_progress_changed(ap, progress):
    $ButtonDiagonal.set_progress(ap, progress)

func _on_same_values_progress_changed(ap, progress):
    $ButtonSameValues.set_progress(ap, progress)

func _on_Abilities_prime_factor_changed(factor):
    print("_on_Abilities_prime_factor_changed ", factor)
    if factor > 1:
        $ButtonPrime.annotate("*%.1f" % factor)
        $ButtonPrime.enable(true)
    else:
        $ButtonPrime.annotate("")
        $ButtonPrime.enable(false)

func _on_Abilities_prime_progress_changed(ap, progress):
    $ButtonPrime.set_progress(ap, progress)

func _on_Abilities_prime_timeout():
    $ButtonPrime.enable(false)
    $ButtonPrime.annotate_suffix("")
    
func _on_Abilitites_prime_tick(second):
    $ButtonPrime.annotate_suffix("[%d]" %second)

func _on_ButtonDiagonal_pressed():
    emit_signal("use_ability_diagonal")

func _on_ButtonFactor_pressed():
    emit_signal("use_ability_factor")

func _on_ButtonMultiply_pressed():
    emit_signal("use_ability_multiply")

func _on_ButtonSameValues_pressed():
    emit_signal("use_ability_same_values")

func _on_ButtonPrime_pressed():
    emit_signal("use_ability_prime")
