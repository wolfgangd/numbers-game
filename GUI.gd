extends Node2D

var passive_total

func _ready():
    $Total.init()
    $RemainingMoves.init("Moves:", 0)
    $AbilityPoints.init("AP:", 0)
    $Highscore.init("High:", 0)

func _enter_tree():
    $HelpOverlay.init(self)
    
func _on_Main_grid_initialized(grid_position, grid_dimensions):
    position = Vector2(grid_position.x + grid_dimensions.x + 5, grid_position.y)
    

func _on_Main_total_changed(total):
    $Total.update_value(total)
    
func _on_Main_remaining_moves(moves):
    $RemainingMoves.update_value(moves)
    
func _on_ability_points_changed(ap):
    $AbilityPoints.update_value(ap)

func _on_Main_highscore_changed(value):
    $Highscore.update_value(value)    

func _on_selected_cell_changed(cell):
    $AdditionPreview.init()
    $AdditionPreview.set_plus(cell.digit, cell.digit_factor)
    passive_total = 0

func _on_selected_cell_updated(cell):
    $AdditionPreview.init()
    $AdditionPreview. passive_total = passive_total
    for factor in cell.total_factors:
        $AdditionPreview.add_factor(factor)
    match cell.op:
        Cell.OP_ADD:
            if cell.total_factors.empty():
                $AdditionPreview.set_plus(cell.digit, cell.digit_factor)
            elif cell.digit_factor > 1:
                $AdditionPreview.add_factor(cell.digit_factor)

        Cell.OP_MULTIPLY:
            $AdditionPreview.add_factor(cell.digit)
            if cell.digit_factor > 1:
                $AdditionPreview.add_factor(cell.digit_factor)
    $AdditionPreview.render_passive_total()


func _on_passive_total_changed(value):
    passive_total = value
