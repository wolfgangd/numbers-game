extends Node


signal factor_changed
signal factor_progress_changed

var factor
var factor_cost

var ability_points_to_factor = {
    3: {"value": 2, "cost": 2},
    6: {"value": 3, "cost": 4},
    10: {"value": 4, "cost": 7},
    15: {"value": 5, "cost": 11},
    21: {"value": 6, "cost": 16}
}


var helpers

func init(helpers_):
    helpers = helpers_
    tick()

func use(cell):
    cell.set_factor(factor)
    return factor_cost

func tick():    
    var data = helpers.get_ability_data(ability_points_to_factor)
    factor_cost = data["cost"]
    factor = data["value"]
    emit_signal("factor_progress_changed", helpers.ability_points, helpers.get_ability_progress(ability_points_to_factor))
    emit_signal("factor_changed", factor)
