extends Node
var ability_points = 0

func set_ability_points(value):
    ability_points = value

func get_ability_data(dict):
    var data = {"cost": 0, "value": 0}
    for k in dict:
        if ability_points >= k:
            data = dict[k]
        else:
            break
    return data


func get_ability_cost(dict):
    var cost = 0
    for k in dict:
        if ability_points >= k:
            cost = k
        else:
            break
    return cost
    
func get_ability_progress(dict):
    var prev = 0
    for k in dict:
        if ability_points < k:
            return [prev, k]
        else:
            prev = k
    return [prev, dict.keys()[dict.keys().size()-1]]
