extends Node

var active_cells = []
var ability_cost = 0

var ability_points_to_same_values = {
    3: {"value": 3, "cost": 2},
    6: {"value": 4, "cost": 4},
    10: {"value": 6, "cost": 7},
    21: {"value": 8, "cost": 11}
}

signal same_values_progress_changed
signal same_value_max_changed
signal ability_enabled_same_values
signal passive_total_changed

var helpers
var selected_cell

func init(helpers_):
    helpers = helpers_
    tick()

func tick():
    var same_values_progress = helpers.get_ability_progress(ability_points_to_same_values)
    var data = helpers.get_ability_data(ability_points_to_same_values)
    var max_usable_value = data["value"]
    ability_cost = data["cost"]
    
    emit_signal("same_values_progress_changed", helpers.ability_points, same_values_progress)
    emit_signal("same_value_max_changed", max_usable_value)
    emit_signal("ability_enabled_same_values", 
                active_cells.empty() && \
                selected_cell && selected_cell.digit <= max_usable_value && \
                same_values_progress[0] > 0 && helpers.ability_points >= same_values_progress[0])
    
func get_active_sum():
    var total = 0
    for cell in active_cells:
        var factor = selected_cell.digit_factor
        total += cell.digit*factor
        cell.used = true
    return total
        
func use(cells):
    for cell in cells:        
        cell.mark_as_same_value()
    active_cells = cells
    emit_signal("passive_total_changed", get_active_sum())
    return ability_cost

func _on_selected_cell_changed(cell):
    active_cells = []
    selected_cell = cell
