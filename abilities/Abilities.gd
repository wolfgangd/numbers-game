extends Node

signal ability_points_changed
signal ability_enabled_diagonal_move
signal diagonal_progress_changed

var ability_points_tick
var ability_points
    
var diagonal_cost = 2

const Helpers = preload("Helpers.gd")

var helpers

func _ready():
    start()
    
func start():
    helpers = Helpers.new()
    $Factor.init(helpers)
    $SameValues.init(helpers)
    $Multiply.init(helpers)
    $Prime.init(helpers)
    ability_points = 0
    ability_points_tick = 0
    calculate_abilities()

func tick():
    ability_points_tick += 1
    if ability_points_tick == 2:
        ability_points += 1
        ability_points_tick = 0
    calculate_abilities()  
              
func get_passive_total():
    return $SameValues.get_active_sum()            
            
func use_diagonal():
    ability_points -= diagonal_cost
    emit_signal("ability_points_changed", ability_points)

func use_multiply(cell):
    ability_points -= $Multiply.use(cell)
    calculate_abilities()

func use_factor(cell):
    ability_points -= $Factor.use(cell)
    calculate_abilities()
    
func use_same_values(active_cells):
    ability_points -= $SameValues.use(active_cells)
    calculate_abilities()

func use_prime(cell, total):
    ability_points -= $Prime.use(cell, total)
    calculate_abilities()

func calculate_abilities():
    helpers.set_ability_points(ability_points)
    
    $Factor.tick()
    $Multiply.tick()
    $SameValues.tick()
    $Prime.tick()
   
    emit_signal("diagonal_progress_changed", ability_points, [0, diagonal_cost])
    emit_signal("ability_points_changed", ability_points)
    emit_signal("ability_enabled_diagonal_move", ability_points >= diagonal_cost)
