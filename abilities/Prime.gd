extends Node

signal prime_progress_changed
signal prime_factor_changed
signal prime_timeout
signal prime_tick

const PrimeNumbers = preload("PrimeNumbers.gd")

var ability_points_to_factor = {
    2: {"value": 3, "cost": 1},
    6: {"value": 3.5, "cost": 3},
    11: {"value": 4.1, "cost": 6},
    17: {"value": 4.8, "cost": 10},
    24: {"value": 5.6, "cost": 15}
}

var ability_cost
var factor
var helpers
var prime_numbers
var timer_last_sec
var was_used

func _process(_delta):
    if !$Timer.is_stopped():
        if ceil($Timer.time_left) != timer_last_sec:
            timer_last_sec = ceil($Timer.time_left)
            emit_signal("prime_tick", timer_last_sec)

func init(helpers_):
    helpers = helpers_
    prime_numbers = PrimeNumbers.new()
    was_used = false
    tick()

func use(cell, total):
    $Timer.stop()
    emit_signal("prime_timeout")
    if prime_numbers.is_prime(total):
        cell.add_total_factor(factor)
    else:
        cell.add_total_factor(0.5)
    was_used = true
    factor = 1
    return ability_cost

func tick():    
    var ability_data = helpers.get_ability_data(ability_points_to_factor)
    ability_cost = ability_data["cost"]
    print("Prime.tick ", ability_data)
    
    if !was_used:
        factor = ability_data["value"]
    was_used = false
    
    print("Prime.tick  factor ", factor)
    
    if factor > 1:
        $Timer.start(10)
        timer_last_sec = -1

    emit_signal("prime_progress_changed", helpers.ability_points, helpers.get_ability_progress(ability_points_to_factor))
    emit_signal("prime_factor_changed", factor)

func _on_Timer_timeout():
    emit_signal("prime_timeout")
    
