extends Node

class_name Multiply

signal multiply_chance_changed
signal multiply_progress_changed
signal multiply_outcome

enum {MULTIPLY, HALF, ADD}

var multiply_chance
var multiply_cost
var was_success

var ability_points_to_mult_chance = {    
    3: { "value": 30, "cost": 2 },
    6: { "value": 40, "cost": 3 },
    10: { "value": 50, "cost": 5 },
    15: { "value": 60, "cost": 8 },
    21: { "value": 80, "cost": 12 },
    28: { "value": 100, "cost": 17 }
}    

var helpers

func init(helpers_):
    helpers = helpers_
    was_success = false
    tick()


func use(cell):
    var mult_roll = randi()%100
    var half_roll = randi()%100
    print("multiply_chance ", multiply_chance)
    print("multiply roll ", mult_roll)
    print("half roll ", half_roll)
    var outcome
    if mult_roll < multiply_chance:
        cell.set_op(Cell.OP_MULTIPLY)
        outcome = MULTIPLY
        was_success = true
    elif half_roll < 100 - multiply_chance:
        cell.add_total_factor(0.5)
        outcome = HALF
    else:
        cell.set_op(Cell.OP_ADD)
        outcome = ADD
    emit_signal("multiply_outcome", cell, outcome)
    if multiply_chance > 0:
        return multiply_cost
    else:
        return 0

func tick():    
    if !was_success:
        var data = helpers.get_ability_data(ability_points_to_mult_chance)
        multiply_cost = data["cost"]
        multiply_chance = data["value"]
        
        emit_signal("multiply_chance_changed", multiply_chance)
        emit_signal("multiply_progress_changed", helpers.ability_points, helpers.get_ability_progress(ability_points_to_mult_chance))

func _on_roller_effect_finished(cell):
    print("roll finished ", cell)
    if was_success:
        emit_signal("multiply_chance_changed", 0)


func _on_selected_cell_changed(_cell):
    was_success = false
