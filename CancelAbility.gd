extends Node2D

signal ability_cancelled

func _ready():
    $Button.visible = false

func _on_Main_diagonal_move_selection(_targets):
    $Button.visible = true
    $Button.text = "Cancel"

func _on_Main_diagonal_move_completed(_targets):
    $Button.visible = false

func _on_Button_pressed():
    emit_signal("ability_cancelled")
